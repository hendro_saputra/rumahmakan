<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $num = 10;
        // for ($i=0; $i <$num ; $i++) {
        //     DB::table('customers')->insert([
        //         'name'      => 'customer',
        //         'email'     => 'admin@dev.com',
        //         'password'  => bcrypt('admin123'),
        //     ]);
        // }
        DB::table('users')->insert([
            'name'=> 'Admin',
            'avatar'=> 'restaurant.jpg',
            'email'=> 'admin@dev.com',
            'password'=> bcrypt('admin123'),
            'gender'=> 'L',
            'city'=> 'Surabaya',
            'phone'=> '083856217199',
            'address'=> 'jalan tohjoyo no 5',
        ]);
        $this->call(PaymentMethodSeeder::class);
    }
}
