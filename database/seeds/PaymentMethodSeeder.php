<?php

use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('payment_methods')->insert([
            'id' => 1,
            'name' => 'Kartu Kredit',
            'method' => 'credit_card',
            'image' => 'visa_master.png',
            'enabled' => 1,
        ]);

        DB::table('payment_methods')->insert([
            'id' => 2,
            'name' => 'Mandiri Clickpay',
            'method' => 'mandiri_clickpay',
            'image' => 'mandiriclickpay.png',
            'enabled' => 1,
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 3,
            'name'          => 'Mandiri Bill Payment',
            'method'        => 'echannel',
            'image'         => 'mandiri.png',
            'enabled'       => 1,
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 4,
            'name'          => 'Permata VA',
            'method'        => 'bank_transfer',
            'image'         => 'permatabank.png',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 5,
            'name'          => 'Transfer Bank Lain',
            'method'        => 'bank_transfer',
            'image'         => 'bank_lain.png',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 6,
            'name'          => 'Mandiri Syariah',
            'method'        => 'manual_mandiri_syariah',
            'image'         => 'ic_bank_mandiri_syariah.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 7,
            'name'          => 'BNI Syariah',
            'method'        => 'manual_bni_syariah',
            'image'         => 'ic_bni_syariah.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 8,
            'name'          => 'CIMB Niaga Syariah',
            'method'        => 'manual_cimb_niaga_syariah',
            'image'         => 'ic_cimb_niaga_syariah.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 9,
            'name'          => 'BANK Muamalat',
            'method'        => 'manual_bank_muamalat',
            'image'         => 'ic_bank_muamalat.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 10,
            'name'          => 'BANK Mega Syariah',
            'method'        => 'manual_bank_mega_syariah',
            'image'         => 'ic_bank_mega_syariah.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 11,
            'name'          => 'BANK BRI Syariah',
            'method'        => 'manual_bank_bri_syariah',
            'image'         => 'ic_bank_bri_syariah.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 12,
            'name'          => 'BANK BCA',
            'method'        => 'manual_bank_bca',
            'image'         => 'ic_bca.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);

        DB::table('payment_methods')->insert([
            'id'            => 13,
            'name'          => 'BANK Mandiri',
            'method'        => 'manual_bank_mandiri',
            'image'         => 'ic_bank_mandiri.png',
            'payment_type'  => 'MANUAL',
            'enabled'       => 1
        ]);
    }
}
