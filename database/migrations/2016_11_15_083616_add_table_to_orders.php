<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->integer('jarak')->after('jumlah_barang');
            $table->integer('jarak_lebih')->after('jumlah_barang');
            $table->integer('harga_tambahan')->after('jumlah_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('jarak');
            $table->dropColumn('jarak_lebih');
            $table->dropColumn('harga_tambahan');
        });
    }
}