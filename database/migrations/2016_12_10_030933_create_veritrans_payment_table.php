<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeritransPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veritrans_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_code');
            $table->string('status_message');
            $table->string('transaction_id');
            $table->dateTime('transaction_time');
            $table->string('fraud_status');
            $table->string('masked_card');
            $table->integer('gross_amount');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
