<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('city')->nullable();
            $table->string('gender')->nullable();
            $table->string('address')->nullable();
            $table->string('no_ktp')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('no_sim')->nullable();
            $table->string('image')->nullable();
            $table->string('api_token');
            $table->string('fcm_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
