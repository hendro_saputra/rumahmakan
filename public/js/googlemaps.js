var myLatLng,lat,long,map,nama;
lat = $('#latitude');
long = $('#longitude');
nama = $('#name');
$(document).ready(function() {
     initMap();
     function initMap(){
        var myLatLng = new google.maps.LatLng(lat.val(),long.val());
        var namaCustomer = nama.val();
        createMap(myLatLng,namaCustomer);
     }

     function createMap(myLatLng,namaCustomer){
        map = new google.maps.Map(document.getElementById('map'),{
           center : myLatLng,
           zoom : 15,
        });

        var marker = new google.maps.Marker({
            position : myLatLng,
            map : map,
            icon : map,
            title : namaCustomer,
        });
     }
});
