<?php

Route::group(['middleware' => 'guest'], function() {
    Route::get('/login', ['as'=>'auth.login' , 'uses' => 'Backend\AuthController@login']);
    Route::post('/login',[
        'as'=>'auth.postLogin', 'uses' => 'Backend\AuthController@postLogin']);
});
Route::group([
        'prefix'        => 'admin',
        'namespace'     => 'Backend',
        'middleware'    => 'auth'
    ],function() {
    Route::get('/', ['uses' => 'AuthController@index', 'as' => 'index']);
    Route::get('/logout',['as'=> 'auth.logout','uses'=> 'AuthController@logout']);
    Route::resource('/food', 'FoodController',
        [
            'except' => ['destroy']
        ]);
    Route::get('food/{id}/delete', ['as'=>'food.delete','uses'=>'FoodController@delete']);
    Route::get('customer/',['as'=> 'customer.index', 'uses'=>'CustomerController@index']);
    Route::get('customer/{id}/show',[
        'as'    => 'customer.show',
        'uses'  =>'CustomerController@show'
    ]);
    Route::get('customer/{id}/edit',[
        'as'    =>'customer.edit',
        'uses'  =>'CustomerController@edit'
    ]);
    Route::put('customer/{id}/update',[
        'as'    => 'customer.update',
        'uses'  => 'CustomerController@update'
    ]);
    Route::get('customer/{id}/delete',[
        'as'    => 'customer.delete',
        'uses'  => 'CustomerController@delete',
    ]);
    Route::get('driver/', [
        'as'    =>'driver.index',
        'uses'  =>'DriverController@index'
    ]);
    Route::get('driver/create', [
        'as'    =>'driver.create',
        'uses'  =>'DriverController@create'
    ]);
    Route::post('driver/store', [
        'as'    =>'driver.store',
        'uses'  =>'DriverController@store'
    ]);
    Route::get('driver/{id}/show',[
        'as'    => 'driver.show',
        'uses'  => 'DriverController@show'
    ]);
    Route::get('driver/{id}/edit',[
        'as'    => 'driver.edit',
        'uses'  => 'DriverController@edit'
    ]);
    Route::put('driver/{id}/update',[
        'as'    => 'driver.update',
        'uses'  => 'DriverController@update'
    ]);
    Route::get('driver/{id}/delete',[
        'as'    => 'driver.delete',
        'uses'  => 'DriverController@delete'
    ]);
    Route::get('orders/index', [
        'as'=> 'order.index',
        'uses'=>'OrderController@index'
    ]);
    Route::get('orders/{id}/show', [
        'as'=> 'order.show',
        'uses'=>'OrderController@show'
    ]);
    Route::get('order/{id}/delete', [
        'as' => 'order.delete',
        'uses' => 'OrderController@delete'
    ]);
    Route::get('pembayaran/index', [
        'as' => 'pembayaran.index',
        'uses' => 'PembayaranController@index'
    ]);

    Route::get('pembayaran/{id}/change-status/{status}/order-id/{orderid}', [
        'as' => 'pembayaran.change',
        'uses' => 'PembayaranController@changeStatus'
    ]);

    Route::get('pengiriman/index', ['as'=>'pengiriman.index','uses'=>'PengirimanController@index']);
    // Route::get('guzzle/index', ['as'=>'pengiriman.index','uses'=>'TalithaController@belajarGuzzle']);
});
