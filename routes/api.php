<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function() {
    Route::post('/auth/customer/register', 'AuthController@registerCustomer');
    Route::post('/auth/customer/login', 'AuthController@loginCustomer');
    Route::post('/auth/driver/register', 'AuthController@registerDriver');
    Route::post('/auth/driver/login', 'AuthController@loginDriver');
    Route::get('/searchcustomer','CustomerController@getPosition');
    Route::group(['middleware' => ['auth:customers_api','api']], function() {
        Route::get('/food', ['uses' =>'FoodController@index']);
        Route::post('customer/set-fcm','CustomerController@setFcm');
        Route::post('/history-pembayaran','CustomerController@historyPembayaran');
        Route::post('/history-pembayaran-detail','CustomerController@historyPembayaranDetail');
        Route::post('/order/add', 'OrderController@addOrder');
        Route::get('/food/{id}/show','FoodController@show');
        Route::get('order/{id}/show', 'OrderController@show');
        Route::post('pembayaran/add', 'PembayaranController@add');
        Route::get('/payment', ['uses' =>'PembayaranController@shoAllPayment']);
        Route::post('payment/change-status', 'PembayaranController@changeStatus');
        Route::get('pembayaran/index', 'PembayaranController@index');
    });
    Route::group(['middleware' => 'auth:drivers_api','api'], function() {
        Route::post('driver/set-fcm','DriverController@setFcm');
        Route::get('pengiriman/all', 'PengirimanController@index');
        Route::post('create/pengiriman', 'PengirimanController@addPengiriman');
        Route::post('change-status/pengiriman', 'PengirimanController@changeStatusPengiriman');
    });

    Route::post('payment/notification', 'PembayaranController@paymentNotification');
    Route::get('payment/notification', 'PembayaranController@paymentNotification');
    Route::get('payment/finish', 'PembayaranController@paymentFinish');

    Route::post('test-fcm', 'TestController@push');
    Route::get('guzzle/index', 'TestController@belajarGuzzle');
});

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');