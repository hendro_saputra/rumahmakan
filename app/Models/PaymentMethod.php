<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('image', function(Builder $builder) {
            $path       = env('PAYMENT_METHOD_IMAGE');
            $builder->select('*')
                    ->addSelect(new \Illuminate\Database\Query\Expression(
                        "CONCAT_WS(image, '{$path}', '') AS image"
                    )
            );
        });
    }
}
