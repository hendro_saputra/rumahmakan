<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class VeritransPayment extends Model
{
    protected $table = 'veritrans_payments';
    public $timestamps = false;

    protected $fillable = [
        'status_code',
        'status_message',
        'transaction_id',
        'transaction_time',
        'fraud_status',
        'masked_card',
        'gross_amount',
        'pembayaran_id'
    ];

    protected $dates = [
        'transaction_time'
    ];

    public function createData($params,$pembayaranId)
    {
        $time = Carbon::parse($params['transaction_time']);
        $data = $this->create([
            'pembayaran_id' => $pembayaranId,
            'status_code' => $params['status_code'],
            'status_message' => $params['status_message'],
            'transaction_id' => $params['transaction_id'],
            'transaction_time' => $time,
            'transaction_status'=> $params['transaction_status'],
            'fraud_status' => (isset($params['fraud_status']) ? $params['fraud_status'] : null),
            'masked_card' => (isset($params['masked_card']) ? $params['masked_card'] : null),
        ]);

        return $data;
    }
}
