<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DateTime;
use Auth;
use File;

class Driver extends Authenticatable
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'city',
        'gender',
        'address',
        'no_ktp',
        'no_telp',
        'no_sim',
        'image',
        'fcm_id',
        'api_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'drivers';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('image', function(Builder $builder) {
            $path       = env('DRIVER_IMAGE');
            $builder->select('*')
                    ->addSelect(new \Illuminate\Database\Query\Expression(
                        "CONCAT_WS(image, '{$path}', '') AS image"
                    )
            );
        });
    }

    public function register($request)
    {
        $apiKey = hash('Sha256',(new DateTime())
            ->format('Y-m-d H:i:s').$request->get('email')
        );

        $data = $this->create([
            'name'      => $request->get('name'),
            'email'     => $request->get('email'),
            'password'  => bcrypt($request->get('password')),
            'city'      => $request->get('city'),
            'gender'    => $request->get('gender'),
            'address'   => $request->get('address'),
            'no_telp'   => $request->get('no_telp'),
            'no_ktp'    => $request->get('no_ktp'),
            'no_sim'    => $request->get('no_sim'),
            'image'     => $request->get('image'),
            'api_token' => $apiKey,
            'fcm_id'    => $request->get('fcm_id'),
        ]);

        return $data;
    }

    public function login($request)
    {
        $driver = auth()->guard('drivers');
        $credentials = [
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        ];

        if ($driver->attempt($credentials)) {
            $id = Auth::guard('drivers')->user()->id;
            $driver = $this->find($id);

            return $driver;
        }else {
            return false;
        }
    }

    public function insert($request)
    {
        $image      = $request->file('image');
        $filename  = md5($image).time().'.'.$image->getClientOriginalExtension();
        $directory  = 'uploads/drivers/';
        $image->move($directory,$filename);

        $apiKey = hash('Sha256', (new DateTime())->format('Y-m-d H:i:s').
                            $request->get('email'));

        $driver = $this->create([
            'name'      => $request->get('name'),
            'email'     => $request->get('email'),
            'password'  => password_hash($request->get('name'),
                            PASSWORD_DEFAULT),
            'image'     => $filename,
            'city'      => $request->get('city'),
            'gender'    => $request->get('gender'),
            'address'   => $request->get('address'),
            'no_ktp'    => $request->get('no_ktp'),
            'no_telp'   => $request->get('no_telp'),
            'no_sim'    => $request->get('no_sim'),
            'api_token' => $apiKey,
        ]);

        return $driver;
    }

    public function updateDriver($request,$id)
    {
        $driver = $this->find($id);
        $image = $request->file('image');

        if ($image != null && $image != '') {
            $directory = 'uploads/drivers/';
            $oldFotoName = basename($driver->image);
            $oldFotoLocation = public_path($directory.$oldFotoName);
            $nameImage = md5(uniqid('update_').time()).'.'.$image->getClientOriginalExtension();

            if ($driver->image == null) {
                $nameImage = md5(uniqid('update_').time()).'.'.$image->getClientOriginalExtension();
            }else {
                if (File::isFile($oldFotoLocation)) {
                    File::delete($oldFotoLocation);
                }else {
                    $nameImage = $oldFotoName;
                }
            }

            $image->move($directory,$nameImage);
            $driver->image = $nameImage;
        }

        if ($request->get('password')!=null && $request->get('password')!= '') {
            $driver->password  = password_hash($request->get('password'),PASSWORD_DEFAULT);
        }

            $driver->name      = $request->get('name');
            $driver->email     = $request->get('email');
            $driver->city      = $request->get('city');
            $driver->gender    = $request->get('gender');
            $driver->address   = $request->get('address');
            $driver->no_ktp    = $request->get('no_ktp');
            $driver->no_telp   = $request->get('no_telp');
            $driver->no_sim    = $request->get('no_sim');

            $driver->save();
    }
}
