<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DateTime;
use File;
use Auth;

class Customer extends Authenticatable
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'image',
        'address',
        'gender',
        'city',
        'fcm_id',
        'api_token',
        'latitude',
        'longitude'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('image', function(Builder $builder) {
            $path       = env('CUSTOMER_IMG_URL');
            $builder->select('*')
                    ->addSelect(new \Illuminate\Database\Query\Expression(
                        "CONCAT_WS(image, '{$path}', '') AS image"
                    )
            );
        });
    }

    public function register($request)
    {
        $apiKey = hash('Sha256',(new DateTime())
            ->format('Y-m-d H:i:s').$request->get('email')
        );

        $data = $this->create([
            'name'       => $request->get('name'),
            'email'      => $request->get('email'),
            'password'   => bcrypt($request->get('password')),
            'phone'      => $request->get('phone'),
            'image'      => $request->get('image'),
            'address'    => $request->get('address'),
            'gender'     => $request->get('gender'),
            'city'       => $request->get('city'),
            'birthday'   => $request->get('birthday'),
            'gcm_id'     => $request->get('gcm_id'),
            'api_token'  => $apiKey,
            'latitude'   => $request->get('latitude'),
            'longitude'  => $request->get('longitude'),
        ]);

        return $data;
    }

    public function login($request)
    {
        $customer = Auth::guard('customers');
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if ($customer->attempt($credentials)) {
            $id = Auth::guard('customers')->user()->id;
            $user = $this->find($id);

            return $user;
        }else {
            return false;
        }
    }

    public function updateData($request,$id)
    {
        $customer = $this->find($id);
        $email  = $request->get('email');
        $customer->name     = $request->get('name');
        $customer->email    = $email;

        if ($request->file('image')!== '' && $request->file('image')!== null) {
            $image              = $request->file('image');
            $directory          = 'uploads/customers/';
            $oldFotoName        = basename($customer->image);
            $oldFotoLocation    = public_path($directory.$oldFotoName);
            $nameNewImage       = uniqid('edit_').md5($email).'.'.
                                    $image->getClientOriginalExtension();
            if ($customer->image == null) {
                $nameNewImage = md5(uniqid('update_').time()).'.'.$image->getClientOriginalExtension();
            }else {
                 if (File::isFile($oldFotoLocation)) {
                    File::delete($oldFotoLocation);
                }else {
                    $nameNewImage = $oldFotoName;
                }
            }

            $image->move($directory,$nameNewImage);
            $customer->image = $nameNewImage;
        }

        if ($request->get('password')!=null && $request->get('password')!= '') {
            $customer->password  = password_hash($request->get('password'),
                                    PASSWORD_DEFAULT);
        }

        $customer->phone    = $request->get('phone');
        $customer->address  = $request->get('address');
        $customer->gender   = $request->get('gender');
        $customer->city     = $request->get('city');

        $customer->save();
    }

    public function setFcmNew($request)
    {
        $id = Auth::user()->id;
        $fcm = $this->find($id);
        if ($request->get('fcm_id')!==null && $request->get('fcm_id')!=='') {
            $fcm->fcm_id = $request->get('fcm_id');
        }

        $fcm->save();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}