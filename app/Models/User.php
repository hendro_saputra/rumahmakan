<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DateTime;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'avatar',
        'password',
        'is_admin',
        'phone',
        'address',
        'gender',
        'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function register($request)
    {
        $apiKey = hash('Sha256',(new DateTime())
            ->format('Y-m-d H:i:s').$request->get('email')
        );

        $data = $this->create([
            'name'      => $request->get('name'),
            'password'  => bcrypt($request->get('password')),
            'api_token' => $apiKey,
            'email'     => $request->get('email'),
            'avatar'    => $request->get('avatar'),
            'is_admin'  => $request->get('is_admin'),
            'phone'     => $request->get('phone'),
            'address'   => $request->get('address'),
            'gcm_id'    => $request->get('gcm_id'),
            'gender'    => $request->get('gender'),
            'city'      => $request->get('city')
        ]);

        return $data;
    }

    public function login($request)
    {
         $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if (Auth::attempt($credentials)) {
            $user = $this->find(Auth::id());

            return $user;
        }else {
            return false;
        }
    }
}
