<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Image;
use File;

class Food extends Model
{
    protected $fillable = [
        'name_food',
        'price',
        'discount',
        'stock',
        'image',
        'description',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('image', function(Builder $builder) {
            $path       = env('FOOD_IMG_URL');
            $builder->select('*')
                    ->addSelect(new \Illuminate\Database\Query\Expression(
                        "CONCAT_WS(image, '{$path}', '') AS image"
                    )
            );
        });
    }

    public function store($request)
    {
        $image      = $request->file('image');
        $filename   = md5($image).time().'.'.$image->getClientOriginalExtension();
        $directory  = 'uploads/foods/';
        $image->move($directory,$filename);

        $food = $this->create([
            'name_food'     => $request->get('name_food'),
            'price'         => $request->get('price'),
            'image'         => $filename,
            'discount'      => $request->get('discount'),
            'stock'         => $request->get('stock'),
            'description'   => $request->get('description'),
        ]);

        return $food;
    }

    public function updateData($request,$id)
    {
        $food = $this->find($id);

        if ($request->get('name_food') !== null && $request->get('name_food') !== '') {
            $food->name_food = $request->get('name_food');
        }

        if ($request->get('price') !== null && $request->get('price') !== '') {
            $food->price = $request->get('price');
        }

        if ($request->file('image') !== null && $request->file('image') !== '') {
            $image              = $request->file('image');
            $directory          = 'uploads/foods/';
            $oldFotoName        = basename($food->image);
            $oldFotoLocation    = public_path($directory.$oldFotoName);
            $nameImage          = uniqid("img_").$image->getClientOriginalName().
                                    '.'.$image->getClientOriginalExtension();

            if (File::isFile($oldFotoLocation)){
                File::delete($oldFotoLocation);
            }else {
                $nameImage = $oldFotoName;
            }

            $image->move($directory,$nameImage);

            $food->image = $nameImage;
        }

        if ($request->get('discount') !== null && $request->get('discount') !== '') {
            $food->discount = $request->get('discount');
        }

        if ($request->get('stock') !== null && $request->get('stock') !== '') {
            $food->stock = $request->get('stock');
        }

        if ($request->get('description') !== null && $request->get('description') !== '') {
            $food->description = $request->get('description');
        }

        $food->save();
    }
}
