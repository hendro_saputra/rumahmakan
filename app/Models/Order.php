<?php

namespace App\Models;

use DB;
use Auth;
use DateTime;
use App\Models\Food;
use App\Models\OrderDetail;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'tanggal',
        'harga_tambahan',
        'jarak_lebih',
        'jarak',
        'latitude',
        'longitude',
    ];
    protected $table = 'orders';

    public $timestamps = false;

    public function addOrder($request)
    {
        $latitude = 0;
        $longitude = 0;

        $stock = $this->checkStokBarang($request->get('data'));

        if (count($stock) !== 0 || !empty($stock)) {
            return $stock;
        }

        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');

        //surabaya
        $latToko = '-7.265757';
        $longToko = '112.734146';

        $distance = $this->getDrivingDistance($latToko,$latitude,$longToko,$longitude);

        $customerCity = implode(',', $distance['customer_city']);
        $customerCity = explode(', ', $customerCity);
        $tokoCity = implode(',', $distance['alamat_toko']);
        $tokoCity = explode(', ', $tokoCity);

        if ($tokoCity[3] !== $customerCity[3]) {
            return $response  = [
                'status' => false ,
                'message' => 'Kota tidak pada area surabaya',
            ];
        }

        $harga = $this->hitungJarakDanHarga($distance['distance']);
        $orderId = $this->addPostOrder($request,$harga,$latToko,$longToko);
        $this->addPostOrderDetails($request,$orderId,$harga);

        $response = [
            'status' => true,
            'order_id' => $orderId,
            'message' => 'Berhasil Order!',
        ];

        return $response;
    }

    public function addPostOrder($request,$harga,$latitude,$longitude)
    {
        $order = $this->create([
                    'customer_id' => Auth::user('customers_api')->id ,
                    'harga_tambahan' => $harga['ongkos_driver'],
                    'jarak' => $harga['jarak'],
                    'tanggal' => (new DateTime())->format('Y-m-d H:i:s'),
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ]);

        return $order->id;
    }

    public function addPostOrderDetails($request,$orderId,$harga)
    {
        $hargaDiskon = 0;
        $discount = 0;
        $totalHarga = 0;
        $subtotal = 0;
        $totalBarang = 0;

        foreach ($request->get('data') as $key) {
            $food = Food::find($key['food_id']);
            $totalBarang += $key['jumlah_barang'];

            if ($food->discount > 0) {
                $discount = (($food->price * $food->discount) / 100);
                $hargaDiskon = $food->price - $discount;
            } else {
                $hargaDiskon = $food->price;
            }

            $stock = $food->stock - $key['jumlah_barang'];
            $food->stock = $stock;
            $food->save();

            $totalHarga += $key['jumlah_barang'] * $hargaDiskon;
            $subtotal = $key['jumlah_barang'] * $hargaDiskon;
            $totalPesananBuatOngkos = $harga['ongkos_driver'] / count($request->get('data'));

            $orderDetail = DB::table('order_details')->insert([
                'order_id' => $orderId,
                'food_id' => $key['food_id'],
                'jumlah_barang' => $key['jumlah_barang'],
                'harga_satuan' => $hargaDiskon,
                'subtotal' => $subtotal ,
            ]);

        }

        $updateOrdersTable = $this->find($orderId);
        $updateOrdersTable->jumlah_barang = $totalBarang;
        $updateOrdersTable->total = $totalHarga + $harga['ongkos_driver'];
        $updateOrdersTable->save();
    }

    public function hitungJarakDanHarga($distance)
    {
        $jarakToko = 1000; //satuan Meter
        $ongkosPerkilometer = 1000;
        if ($distance > $jarakToko) {
            $jumlah = ceil($distance/$jarakToko) * $ongkosPerkilometer;
            $hargaTambahan = $jumlah;
            $jarakLebih = $distance - $jarakToko;

            return [
                'ongkos_driver' => $hargaTambahan,
                'jarak' => $distance
            ];
        }

        return [
            'ongkos_driver' => 0,
            'jarak_lebih' => 0,
            'jarak' => $distance
        ];
    }

    public function checkStokBarang($stock)
    {
        $responseStatus = [];

        foreach ($stock as $key) {
            $food = Food::find($key['food_id']);
            if ($key['jumlah_barang'] > $food->stock ) {
                $response = [
                        'status'    => false,
                        'fodo_id'   => $key['food_id'],
                        'message'   => 'Stock tidak cukup!',
                    ];
                $responseStatus[] = $response;
            }
        }

        return $responseStatus;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'order_id');
    }

    public function food()
    {
        return $this->belongsTo(Food::class);
    }

    public function getDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".
                $lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=id";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        curl_close($ch);
        $responseA = json_decode($response, true);

        $dist = $responseA['rows'][0]['elements'][0]['distance']['value'];
        $time = $responseA['rows'][0]['elements'][0]['duration']['value'];

        $cityCustomer = $responseA['destination_addresses'];
        $alamatToko = $responseA['origin_addresses'];

        return array(
                'distance' => $dist,
                'time' => $time,
                'customer_city' => $cityCustomer,
                'alamat_toko' => $alamatToko);
    }

    public function pembayarans()
    {
        return $this->hasMany(Pembayaran::class);
    }
}