<?php

namespace App\Models;

use Auth;
use DateTime;
use App\Models\Order;
use GuzzleHttp\Client;
use App\Models\PaymentMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Pembayaran extends Model
{
    protected $fillable = [
        'order_id',
        'payment_method_id',
        'payment_status',
        'payment_type',
        'unique_id',
        'total','mark',
        'created_at',
        'redirect_url'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('mark', function(Builder $builder) {
            $path       = env('MARK_IMAGE');
            $builder->select('*')
                    ->addSelect(new \Illuminate\Database\Query\Expression(
                        "CONCAT_WS(mark, '{$path}', '') AS mark"
                    )
            );
        });
    }

    public function postData($request)
    {
        $user = Auth::user('customers_api');
        $client = new Client();
        $response = [];

        foreach ($request->get('data') as $key) {
            $cari = Order::where('id',$key['order_id'])->get();
            if (count($cari) == 0) {
                return [
                    'status' => 400,
                    'message' => 'Order Id Tidak ditemukan'
                ];
            }

            $paymentMethod = PaymentMethod::find($key['payment_method_id']);
            $order = Order::find($key['order_id']);
            $orderDetailsItem = $this->showOrderDetails($key['order_id'],$order);
            $uniqueId = $this->uniqueId();

            if ($paymentMethod->payment_type == "MANUAL") {
                $data = $this->create([
                    'order_id' => $key['order_id'],
                    'payment_method_id' => $key['payment_method_id'],
                    'payment_type' => $paymentMethod->payment_type,
                    'unique_id' => $uniqueId,
                    'total' => $order->total + $uniqueId,
                    'created_at' => (new DateTime())->format('Y-m-d H:i:s')
                ]);

                $pembayaran = Pembayaran::with([
                    'paymentMethod',
                    'order' => function($order){
                        $order->with('orderDetails');
                    }
                ])->find($data->id);
                return $pembayaran;
            }

             $data = $this->create([
                    'order_id' => $key['order_id'],
                    'payment_method_id' => $key['payment_method_id'],
                    'payment_type' => $paymentMethod->payment_type,
                    'unique_id' => 0,
                    'total' => $order->total,
                    'created_at' => (new DateTime())->format('Y-m-d H:i:s')
                ]);

             $pembayaran = Pembayaran::find($data->id);

            if ($paymentMethod->payment_type == 'VERITRANS') {
                $transaction = [
                    'payment_type'  =>'vtweb',
                    'vtweb'=>[
                        'enabled_payments'=> [
                            $paymentMethod->method
                        ],
                        'credit_card_3d_secure'=> true
                    ],
                    'customer_details'  => [
                        'first_name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone
                    ],
                    'transaction_details' => [
                        'order_id' => $key['order_id'],
                        'gross_amount'=> $order->total
                    ],

                    'echannel'=> [
                        'bill_info1' => 'Resto Agung',
                        'bill_info2' => 'Resto Agung',
                        'bill_info3' => $user->name
                    ],

                    'item_details' => $orderDetailsItem,
                ];

                $authVeritrans = 'Basic '.base64_encode(env('VT_SERVER_KEY').':');

                $response = $client->request('POST',env('VERITRANS_DEV').'charge',[
                    'headers'   => [
                        'content-type'  => 'application/json',
                        'Accept'    => 'application/json',
                        'Authorization' => $authVeritrans
                    ],
                    'json' => $transaction,
                ]);

                $response = json_decode($response->getBody()->getContents());
                $pembayaran->redirect_url =  $response->redirect_url;
                $pembayaran->save();
                $pembayaran = $pembayaran = Pembayaran::with([
                    'paymentMethod', 'order' => function($order){
                        $order->with('orderDetails');
                    }
                ])->find($data->id);
                $response = $pembayaran;
            }

            return $response;
        }
    }

    public function uniqueId()
    {
        $manual = $this->where('payment_type','MANUAL')->get();
        $last = $this->where('payment_type','MANUAL')->orderBy('id','DESC')->first();

        if ($manual->count() == 0) {
            return 1;
        }

        if ($last->unique_id == 999) {
            return 1;
        }

        if ($last->unique_id < 999) {
            return $last->unique_id+1;
        }
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function pengiriman()
    {
        return $this->hasOne(Pengiriman::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function showOrderDetails($id,$orderTotal)
    {
        $data = [];
        $dataBarang = [];
        $order = Order::with(['orderDetails' => function($order){
            $order->with('food');
        }])->find($id);

        $ongkosDriver = [
            'id' => $orderTotal->id,
            'name' => 'Ongkos Pengiriman',
            'price' => $orderTotal->harga_tambahan,
            'quantity' => 1
        ];

        foreach ($order->orderDetails as $detail) {
            $data['id'] = $detail->id;
            $data['price'] = $detail->harga_satuan;
            $data['quantity'] = $detail->jumlah_barang;
            $data['name'] = $detail->food->name_food;
            $data['ongkos_driver'] = $detail->ongkos_driver;
            $dataBarang[] = $data;
        }

        $dataBarang[] = $ongkosDriver;

        return $dataBarang;
    }
}
