<?php

namespace App\Models;

use Auth;
use App\Models\Driver;
use App\Models\Pembayaran;
use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $fillable =[
        'pembayaran_id',
        'driver_id',
        'alamat_penerima',
        'nama_penerima',
        'status'
    ];

    protected $table ='pengirimans';

    public function addPengiriman($request,$driverId)
    {
        foreach ($request->get('data') as $data) {
            $data = $this->create([
                'pembayaran_id' => $data['pembayaran_id'],
                'alamat_penerima' => $data['alamat_penerima'],
                'nama_penerima' => $data['nama_penerima'],
                'status' => 0,
                'driver_id' => $driverId
        ]);

            $driver = Driver::find($data->driver_id);
            $driver->status = 1;
            $driver->save();

            $pembayaran = Pembayaran::find($data->pembayaran_id);
            $pembayaran->payment_status = 2;
            $pembayaran->save();
        }

        return $data;
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
}
