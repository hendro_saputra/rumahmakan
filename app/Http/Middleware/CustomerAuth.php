<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CustomerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'customers')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized',401);
            }else {
                return redirect()->guest('/login');
            }
        }
        return $next($request);
    }
}
