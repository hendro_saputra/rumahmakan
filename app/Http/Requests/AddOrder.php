<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'food_id' => 'required',
        ];

        foreach($this->request->get('data') as $key => $val)
        {
            $rules['data.'.$key] = 'required|max:10';
        }

        return $rules;
    }
}
