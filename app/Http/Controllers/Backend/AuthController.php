<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;

class AuthController extends Controller
{
    public function index()
    {
        return view('backend.templates.default');
    }

    public function login(Request $request)
    {
        return view('backend.auth.login', [
            'title' => 'Login User',
        ]);
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required',
        ]);

        if(!Auth::attempt([
            'email'     => $request->email,
            'password'  => $request->password,
        ])) {
            return redirect()->back()->with('danger', 'detail login salah');
        }

        return redirect()->route('index')->with('info', 'Berhasil login');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()
                ->route('auth.login')
                ->with('info', 'You have logged out');
    }

    public function customerPostLogin(Request $request)
    {
        $customer = auth()->guard('customers');

        $credentials = [
            'email'     => $request->email,
            'password'  => $request->password,
        ];

        if ($customer->attempt($credentials)) {
            return redirect('customer');
        }else {
            return 'information access denied';
        }
    }
}
