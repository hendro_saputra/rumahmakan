<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Http\Requests;

class OrderController extends Controller
{
    protected $orderDetail;

    public function __construct()
    {
        $this->orderDetail = new OrderDetail();
    }

    public function index()
    {
        $orders = Order::with('customer')->orderBy('id','desc')->paginate(5);

        return view('backend.order.index',[
            'orders' => $orders
        ]);
    }

    public function show($id)
    {
        $total = 0;
        $details = $this->orderDetail->where('order_id','=',$id)->get();
        $order = Order::find($id);

        foreach ($details as $detail) {
            $total +=$detail->subtotal;
        }

        return view('backend.order.show',[
            'details'   => $details,
            'total'     => $total,
            'order'     => $order,
            'no'        =>1
        ]);
    }

    public function delete($id)
    {
        $order = Order::find($id);
        $order->delete();

        return redirect()->route('order.index')->with('info','Delete Sukses!');
    }
}
