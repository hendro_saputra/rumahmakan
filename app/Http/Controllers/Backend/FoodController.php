<?php

namespace App\Http\Controllers\Backend;

use File;
use DB;
use App\Models\Food;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FoodController extends Controller
{
    protected $food;

    public function __construct()
    {
        $this->food = new Food();
    }

    public function index(Request $request)
    {
        $nama = DB::table('foods')->where('name_food','LIKE','%'.$request->name.'%')
                    ->paginate(5);
        $food = $nama;

        return view('backend.food.index',[
            'foods' => $food
        ]);
    }

    public function create()
    {
        return view('backend.food.add');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name_food' => 'required',
            'price'     => 'required|numeric',
            'image'     => 'required|image',
            'stock'     => 'required'
        ]);

        $this->food->store($request);

        return redirect()->route('food.index')
                ->with('success','Makanan berhasil di tambahkan');
    }

    public function show($id)
    {
        $food = $this->food->find($id);
        return view('backend.food.show',[
            'food' => $food
        ]);
    }

    public function edit($id)
    {
        $food = $this->food->find($id);

        return view('backend.food.edit',[
            'food' => $food
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'image' => 'image'
        ]);

        $this->food->updateData($request,$id);

        return redirect()->route('food.index')->with('success','Makanan Updated');
    }

    public function delete($id)
    {
        $food = $this->food->find($id);

        if (File::isFile($food->image)){
            File::delete($food->image);
        }

        $food->delete();

        return redirect()->route('food.index')
                    ->with('info','berhasil delete makanan');
    }
}
