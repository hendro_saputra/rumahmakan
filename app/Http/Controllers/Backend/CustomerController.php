<?php

namespace App\Http\Controllers\Backend;

use File;
use App\Http\Requests;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    protected $customer;

    public function __construct()
    {
        $this->customer = new Customer;
    }

    public function index()
    {
        $customers = $this->customer->paginate(10);

        return view('backend.customer.index',[
            'customers' => $customers
        ]);
    }

    public function show($id)
    {
        $customer = $this->customer->find($id);

        return view('backend.customer.show',[
            'customer' => $customer
        ]);
    }

    public function edit($id)
    {
        $customer = $this->customer->find($id);

        return view('backend.customer.edit',[
            'customer' => $customer
        ]);
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'name'          => 'required',
            'email'         => 'required',
            'repassword'    => 'same:password|min:5',
            'phone'         => 'required|numeric',
            'address'       => 'required',
            'image'         => 'image',
            'city'          => 'required',
        ];

        $this->validate($request,$rules);
        $this->customer->updateData($request,$id);

        return redirect()->route('customer.index')
                        ->with('info','Berhasil meng update!');
    }

    public function delete($id)
    {
        $customer = $this->customer->find($id);

        if (File::isFile($customer->image)){
            File::delete($customer->image);
        }
        $customer->delete();

        return redirect()->route('customer.index')
                    ->with('info','Berhasil menghapus!');
    }
}
