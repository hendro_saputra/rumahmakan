<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pengiriman;

class PengirimanController extends Controller
{
    public function index()
    {
        $pengiriman = Pengiriman::paginate(10);

        return view('backend.pembayaran.pengiriman',[
            'pengirimans'=>$pengiriman
        ]);
    }
}
