<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Driver;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;

class DriverController extends Controller
{
    protected $driver;

    public function __construct()
    {
        $this->driver = new Driver();
    }

    public function index()
    {
        $drivers = $this->driver->paginate(10);

        return view('backend.driver.index',[
            'drivers' => $drivers
        ]);
    }

    public function create()
    {
        return view('backend.driver.add');
    }

    public function store(Request $request)
    {
        $rules = [
            'name'        => 'required|min:3',
            'email'       => 'required|email|unique:drivers',
            'password'    => 'required|min:3',
            'repassword'  => 'same:password|min:5',
            'image'       => 'required|image',
            'city'        => 'required',
            'gender'      => 'required',
            'address'     => 'required',
            'no_ktp'      => 'required|numeric|min:3',
            'no_telp'     => 'required|numeric|min:3',
            'no_sim'      => 'required|numeric|min:3',
        ];

        $this->validate($request,$rules);
        $this->driver->insert($request);

        return redirect()->route('driver.index')
                ->with('success','Berhasil menambahkan driver!');
    }

    public function show($id)
    {
        $driver = $this->driver->find($id);

        return view('backend.driver.show',[
            'driver' => $driver
        ]);
    }

    public function edit($id)
    {
        $driver = $this->driver->find($id);

        return view('backend.driver.edit',[
            'driver' => $driver
        ]);
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'name'        => 'required|min:3',
            'email'       => 'required|email',
            'repassword'  => 'same:password',
            'image'       => 'image',
            'city'        => 'required',
            'gender'      => 'required',
            'address'     => 'required',
            'no_ktp'      => 'required|numeric|min:3',
            'no_telp'     => 'required|numeric|min:3',
            'no_sim'      => 'required|numeric|min:3',
        ];

        $this->validate($request,$rules);
        $this->driver->updateDriver($request,$id);

        return redirect()->route('driver.index')
                    ->with('info','Berhasil Update!');
    }

    public function delete($id)
    {
        $driver             = $this->driver->find($id);
        $oldFotoLocation    = $driver->image;

        if (File::isFile($oldFotoLocation)){
            File::delete($oldFotoLocation);
        }

        $driver->delete();

        return redirect()->route('driver.index')
                    ->with('info','berhasil delete driver');
    }
}