<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TalithaController extends Controller
{
    public function talitha()
    {
        return view('backend.templates.talitha');
    }
}
