<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Fcm;
use App\Models\Order;
use App\Models\Driver;
use App\Models\Customer;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PembayaranController extends Controller
{
    protected $status;

    public function index()
    {
        $pembayaran = Pembayaran::orderBy('id','desc')->with(['order' => function($order){
            $order->with('customer');
        }])->paginate(10);

        // $pdf = view('others.pdf', $data);

        return view('backend.pembayaran.index',['pembayaran' => $pembayaran]);
    }

    public function changeStatus($id,$status,$orderId)
    {
        $fcm = new Fcm();
        $pembayaran = Pembayaran::find($id);
        $order = Order::with('orderDetails')->find($orderId);
        $drivers = Driver::all();
        $tokenUser = [];

        foreach ($drivers as $driver) {
            $tokenUser[] = $driver->fcm_id;
        }

        $message = [
            'title'=>'Order Baru',
            'body'=> $order
        ];

        $fcm->sendMessage($tokenUser,$message);

        $pembayaran->payment_status = $status;
        $pembayaran->save();

        return redirect()->route('pembayaran.index');
    }
}
