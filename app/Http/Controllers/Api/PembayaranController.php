<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Fcm;
use App\Http\Requests;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use App\Models\VeritransPayment;
use App\Http\Controllers\Controller;
use App\Models\PaymentMethod as payment;

class PembayaranController extends Controller
{
    protected $payment,$pembayaran;

    public function __construct()
    {
        $this->payment = new PaymentMethod();
        $this->pembayaran = new Pembayaran();
    }

    public function index()
    {
        $pembayarans = Pembayaran::where('payment_status','=', 1)->get();

        return response()->json([
            'status' => 200,
            'data' => $pembayarans
        ],200);
    }

    public function add(Request $request)
    {
        $this->validate($request,[
            'data.*.order_id' => 'required',
            'data.*.payment_method_id'=> 'required',
        ]);

        $pembayaran =  $this->pembayaran->postData($request);

        return response()->json([
            'status' => 200,
            'message' => $pembayaran
        ],200);
    }

    public function shoAllPayment()
    {
        $payment = payment::all();

        return response()->json(['status'=>200,'data'=>$payment],200);
    }

    public function changeStatus(Request $request)
    {
        $fcm = new Fcm();

        $fileName='';
        $pembayaran = Pembayaran::find($request->get('id'));

        if ($request->hasFile('image')) {
            $fileName = $this->upload($request->file('image'),'pembayarans');
        }
        $pembayaran->mark = $fileName;
        $pembayaran->save();

        return response()->json(['status'=>200,'message'=>'sukses']);
    }

    public function upload($image,$path)
    {
        $filename  = md5($image).time().'.'.$image->getClientOriginalExtension();
        $directory  = 'uploads/'.$path.'/';
        $image->move($directory,$filename);

        return $filename;
    }

    public function paymentNotification(Request $request)
    {
        $fcm = new Fcm();
        $params = $request->all();
        if ($params['status_code'] == '200'
            && ($params['transaction_status'] == 'capture'
                ||
                $params['transaction_status'] == 'settlement')
            || $params['fraud_status'] == 'accept'
        ) {

            $orderId = (int)$params['order_id'];
            $veritransPayment = new VeritransPayment();
            $pembayaran = Pembayaran::where('order_id',$orderId)->first();
            $veritrans = $veritransPayment->createData($params,$pembayaran->id);
            $pembayaranChange = Pembayaran::find($pembayaran->id);
            $pembayaranChange->payment_status = 1;
            $pembayaranChange->save();

            $data = $this->fcmSendMessage($orderId);

            return response()->json([
                'status'=>200,
                'message'=> $veritrans,
            ],200);
        }

        return response()->json([
            'status' => 500,
            'message'=>'failed'
        ],500);
    }

    public function paymentFinish(Request $request)
    {
        return response()->json([
            'status'=>200,
            'data'=>$request->all()
        ]);
    }

    public function fcmSendMessage($orderId)
    {
        $fcm = new Fcm();
        $tokenUser = [];
        $drivers = Driver::all();
        $order = Order::with('orderDetails')->find($orderId);

        foreach ($drivers as $driver) {
            $tokenUser[] = $driver->fcm_id;
        }

        $message = [
            'title' => 'Order Baru',
            'body' => $order,
        ];

        $fcm->sendMessage($tokenUser,$message);
    }
}
