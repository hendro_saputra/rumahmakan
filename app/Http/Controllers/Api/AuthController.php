<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Driver;

class AuthController extends Controller
{
    protected $customer,$driver;

    public function __construct()
    {
        $this->middleware('api');
        $this->customer = new Customer();
        $this->driver   = new Driver();
    }

    public function registerCustomer(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required|min:3',
            'email'         => 'required|email|unique:customers',
            'password'      => 'required|min:3',
            'latitude'      => 'required',
            'longitude'     => 'required',
        ]);

        $data = $this->customer->register($request);

        return response()->json(['status'=>201,'data' => $data],201);
    }

    public function loginCustomer(Request $request)
    {
        $this->validate($request,[
            'email'     => 'required',
            'password'  => 'required',
        ]);

        $dataLogin = $this->customer->login($request);
        $gagal   = [
            'status'    => 400,
            'message'   => 'Error, Invalid Data',
        ];

        if ($dataLogin != false) {
            return response()->json(['status' => 200,'data' => $dataLogin,],200);
        }else {
            return response()->json($gagal,400);
        }
    }

    public function loginDriver(Request $request)
    {
        $this->validate($request,[
            'email'     => 'required',
            'password'  => 'required'
        ]);

        $dataLogin = $this->driver->login($request);

        if ($dataLogin ==false) {
            return response()->json(['status'=> 401,'data'=> 'Password dan email salah'],401);
        }
            return response()->json(['status'=> 200,'data'=> $dataLogin],200);

    }

    public function registerDriver(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required',
            'email'     => 'required|email|unique:drivers',
            'password'  => 'required'
        ]);

        $dataLogin = $this->driver->register($request);

        $gagal   = [
            'status'    => 400,
            'message'   => 'Error, Invalid Data',
        ];

        if ($dataLogin != false) {
            return response()->json(['status' => 201,'data' => $dataLogin,],201);
        }else {
            return response()->json($gagal,400);
        }
    }
}
