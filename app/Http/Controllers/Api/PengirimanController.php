<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\Driver;
use App\Models\Pengiriman;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PengirimanController extends Controller
{
    public function addPengiriman(Request $request)
    {
        $pengiriman = new Pengiriman();
        $statuDriver =  Auth::guard('drivers_api')->user()->status;
        $driverId = Auth::guard('drivers_api')->user()->id;

        foreach ($request->get('data') as $dataPengiriman) {
            $pembayaran = Pembayaran::where('id',$dataPengiriman['pembayaran_id'])
                            ->where('payment_status', 0);

            if ($pembayaran->count() == 1) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Customer belom bayar'
                ],404);
            }
        }

        if ($statuDriver == 1) {
            return response()->json([
                'status' => 'gagal',
                'message'=>'driver sedang dalam pengiriman'
            ],400);
        }

        $response = $pengiriman->addPengiriman($request, $driverId);

        return response()->json([
            'status'=>'sukses',
            'message'=> ['berhasil menambah pengiriman', $response]
        ],201);
    }

    public function changeStatusPengiriman(Request $request)
    {
        $driverId = Auth::guard('drivers_api')->user()->id;
        $checkPengiriman = Pengiriman::where('id',$request->get('id'));

        if ($checkPengiriman->count() == 0) {
            return response()->json([
                'status' => 404,
                'message' => 'Id pengiriman tidak ditemukan'
            ], 404);
        }

        $pengiriman = Pengiriman::find($request->get('id'));
        $pembayaran = Pembayaran::find($pengiriman->pembayaran_id);

        $pengiriman->status = $request->get('status');
        $pengiriman->save();

        $pembayaran->payment_status = 3;
        $pembayaran->save();

        $driver = Driver::find($driverId);
        $driver->status = 0;
        $driver->save();

        return response()->json([
            'status' => 200,
            'message' => 'berhasil update status pengiriman'
        ]);
    }

    public function index()
    {
        $order = Pembayaran::where('payment_status',1)->with(['order' => function($orderDetails){
            $orderDetails->with(['customer','orderDetails' => function($food){
                $food->with('food');
            }]);
        }])->paginate(10);

        return response()->json([
                'status'=>200,
                'data'=>['order'=>$order],
            ],200);
    }
}
