<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Fcm;
use App\Models\Driver;
use GuzzleHttp\Client;

class TestController extends Controller
{
    public function push(Request $request)
    {
        $fcm = new Fcm();

        foreach ($request->get('data') as $data) {
            $tokenUser[] = $data['token'];
        }

        $message = ['title'=>'Order Baru','body'=>'Test FCM'];
        $push = $fcm->sendMessage($tokenUser,$message);

        return response()->json(['data'=>$push]);
    }

    public function belajarGuzzle()
    {
        $apikey = '58a2d9ecbad008bd69a376651f5940fd';
        $client = new Client();

        $response = $client->request('GET', 'http://api.rajaongkir.com/starter/province',[
            'query' => [
                'key' => $apikey,
            ],
            'headers' => [
                'content-type' => 'Application/json',
                'accept' => 'Application/json',
            ]
        ]);

        $dataOngkir = json_decode($response->getBody(),true);

        return $dataOngkir;
    }
}
