<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Food;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class FoodController extends Controller
{
    public function index()
    {
        $data = Food::all();

        $responGagal = [
            'status'=> 400,
            'keterangan'=> 'No Data!',
        ];

        if (count($data) > 0) {
            return response()->json([
                'status'=>200,
                'data' => $data
            ],200);
        } else {
            return response()->json($responGagal);
        }
    }

    public function show($id)
    {
        $food = Food::find($id);
        return response()->json(['status'=>200,'data'=>$food],200);
    }
}
