<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\Order;
use App\Http\Requests;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Pembayaran;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    protected $customer;

    public function __construct()
    {
        $this->middleware('api');
        $this->customer = new Customer();
    }

    public function setFcm(Request $request)
    {
        $fcm = $this->customer->setFcmNew($request);

        return response()->json(['status' =>200,'keterangan'=>'sukses update fcm']);
    }

    public function historyPembayaran(Request $request)
    {
        $id = $request->get('user_id');

        $data = Order::where('customer_id', $id)->with('pembayarans')->get();

        return response()->json(["status"=> 200,"data" => $data],200);
    }

    public function historyPembayaranDetail(Request $request)
    {
        $id = $request->get('pembayaran_id');
        $data = Pembayaran::with(['order' => function($order){
            $order->with(['orderDetails' => function($orderDetails){
                $orderDetails->with('food');
            }]);
        }])->find($id);

        return response()->json([
            "status" => 200,
            "data" => $data
        ]);
    }
}
