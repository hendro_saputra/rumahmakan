<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\Food;
use App\Models\Order;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Pembayaran;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $customer,$order;

    public function __construct()
    {
        $this->customer = new Customer();
        $this->order = new Order();
    }

    public function addOrder(Request $request)
    {
        $data = [];

        $this->validate($request,[
            'data.*.food_id'          => 'required',
            'data.*.jumlah_barang'    => 'required|numeric|min:1',
        ]);

        return $data = $this->order->addOrder($request);

        foreach ($data as $key) {
            if($key['status'] === false){
                return response()->json([
                        'status'        => 'Gagal',
                        'Keterangan'    => $data,
                ],400);
            }
            return response()->json($data,201);
        }

    }

    public function show($id)
    {
        $order = Order::with('customer')->with('orderDetails')->find($id);

        return response()->json([
                'status'=>200,
                'data'=> ['order' =>  $order,]
            ]);
    }
}
