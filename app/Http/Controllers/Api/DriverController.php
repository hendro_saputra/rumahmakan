<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Driver;

class DriverController extends Controller
{
    public function setFcm(Request $request)
    {
        $id = Auth::user('drivers_api')->id;
        $driver = Driver::find($id);

        $driver->fcm_id = $request->get('fcm_id');
        $driver->save();

        return response()->json(['status'=>200,'message'=>'success update fcm']);
    }
}
