<?php

namespace App\Helpers;

class Fcm
{
    public function sendMessage($token,$message)
    {
        $url        = 'https://fcm.googleapis.com/fcm/send';
        $serverKey  = 'AIzaSyD_PteCUvHSVBtXga4_3-DI4BiIzl0HPDg';

        $headers = [
            'Content-Type:application/json ',
            'Authorization:key= '.$serverKey
        ];

        /*
        Parameter Example
            $data = array('post_id'=>'12345','post_title'=>'A Blog post');
            $token = 'single tocken id or topic name';
            or
            $token = array('token1','token2','...'); // up to 1000 in one request
        */

        $fields = array();
        $fields['data'] = $message;
        $fields['registration_ids'] = $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Error:'.curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }
}