@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Detail Customers</h1><hr>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="thumbnail">
                                <img src="{{ $customer->image }}" alt="">
                          </div>
                          <a href="{{ route('customer.index') }}" class="btn btn-primary">Kembali</a>
                      </div>
                        <div class="col-md-6">
                          <div class="panel panel-body">
                              <table class="table table-bordered">
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Nama</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $customer->name }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Email</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $customer->email }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>No Hp</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                    {{ $customer->phone }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Address</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                {{ $customer->address }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>Gender</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $customer->gender }}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>City</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $customer->city }}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                          </div>
                        </div>
                  </div>
                  <br>
                  <div class="row">
                        <h3 align="center"><b>Location Customers</b></h3>
                        <div class="form-group">
                            <input type="hidden" id="latitude" name="latitude" value="{{ $customer->latitude }}" class="form-control" style="width: 100px;">
                            <input type="hidden" id="longitude" name="longitude" value="{{ $customer->longitude }}" class="form-control" style="width: 100px;" disabled="disabled">
                            <input type="hidden" id="name" name="name" value="{{ $customer->name }}" class="form-control" style="width: 100px;" disabled="disabled">
                        </div>

                  </div>
                  <div class="thumbnail">
                    <div id="map" style="height: 500px; width: 100%; margin: 0 auto;">

                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
                    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnAI0-Zq-kKqLd-x2ooBDygizVwgjcJpI" type="text/javascript">
                    </script>
                    <script src=" {{ asset('js/googlemaps.js') }} "></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
                  </div>
@endsection