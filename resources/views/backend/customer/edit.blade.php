@extends('backend.templates.default')
@section('content')
    <form action="{{ route('customer.update', $customer->id) }}" method="post" enctype="multipart/form-data">
   {{ method_field('PUT') }}
    <h1 align="center" class="panel panel-heading">Edit Customer</h1>
        <div class="form-group row">
          <label for="name" class="col-xs-2 col-form-label">Name</label>
          <div class="col-xs-10 {{ $errors->has('name') ? ' has-error ' : '' }}">
            <input value="{{ $customer->name }}" class="form-control" type="text" name="name" >
          @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="email" class="col-xs-2 col-form-label">Email</label>
           <div class="col-xs-10 {{ $errors->has('email') ? ' has-error ' : '' }}">
            <input value="{{ $customer->email }}" class="form-control" type="email" name="email">
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="image" class="col-xs-2 col-form-label">Image</label>
          <div class="col-xs-10 {{ $errors->has('image') ? ' has-error ' : '' }}">
            <input class="form-control" type="file" name="image" accept="image/*" id="example-url-input">
          @if ($errors->has('image'))
                <span class="help-block">{{ $errors->first('image') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="password" class="col-xs-2 col-form-label">Password</label>
          <div class="col-xs-10 {{ $errors->has('password') ? ' has-error ' : '' }}">
            <input value="" class="form-control" name="password" type="password" value="" id="example-number-input">
             @if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-2 col-form-label">Re-Password</label>
          <div class="col-xs-10 {{ $errors->has('repassword') ? ' has-error ' : '' }}">
            <input class="form-control" name="repassword" type="password" value="" id="example-repassword-input">
             @if ($errors->has('repassword'))
                <span class="help-block">{{ $errors->first('repassword') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-phone-input" class="col-xs-2 col-form-label">No Hp</label>
          <div class="col-xs-10 {{ $errors->has('phone') ? ' has-error ' : '' }}">
            <input value="{{ $customer->phone }}" class="form-control" name="phone" type="number" value="" id="phone">
             @if ($errors->has('phone'))
                <span class="help-block">{{ $errors->first('phone') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-address-input" class="col-xs-2 col-form-label">Alamat</label>
          <div class="col-xs-10 {{ $errors->has('address') ? ' has-error ' : '' }}">
            <input value="{{ $customer->address }}" class="form-control" name="address" type="text" value="" id="example-address-input">
             @if ($errors->has('address'))
                <span class="help-block">{{ $errors->first('address') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-address-input" class="col-xs-2 col-form-label">Jenis Kelamain</label>
          <div class="col-xs-10 {{ $errors->has('gender') ? ' has-error ' : '' }}">
            <select name="gender" class="form-control">
              <option value="L" @if ($customer->gender == "L")
                  selected="selected "
                @endif>Laki-Laki</option>
              <option value="P" @if ($customer->gender == "P")
                selected="selected "
              @endif>Perempuan</option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="example-address-input" class="col-xs-2 col-form-label">Kota</label>
          <div class="col-xs-10 {{ $errors->has('city') ? ' has-error ' : '' }}">
            <input value="{{ $customer->city }}" class="form-control" name="city" type="text" id="example-city-input">
             @if ($errors->has('city'))
                <span class="help-block">{{ $errors->first('city') }}</span>
            @endif
          </div>
        </div>
        {{ csrf_field() }}
        <input type="submit" class="btn btn-primary col-xs-offset-5">
        <a href="{{ route('customer.index') }}" class="btn btn-warning">Cancel</a>
        </form>
@endsection