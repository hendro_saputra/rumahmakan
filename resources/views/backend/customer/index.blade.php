@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading text-center">Daftar Customers</h1><hr>
              <div class="input-group col-md-3 pull-right">
                {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">
                  <input type="submit" value="Go">
                </span> --}}
              </div>
              <br>

              <br>
              <table class="table table-striped col-md-3">
              <thead>
                <tr>
                  <th>Nama Customer</th>
                  <th>Alamat</th>
                  <th>email</th>
                  <th>Jenis Kelamin</th>
                  <th>Kota</th>
                  <th>No Telephon</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @if ($customers->count() == 0)
                <tr>
                  <td colspan="7" align="center"><h4>Data Kosong!</h4></td>
                </tr>
              @else
              @foreach ($customers as $customer)
                <tr>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->gender }}</td>
                    <td>{{ $customer->city }}</td>
                    <td>{{ $customer->phone }}</td>
                    <td>
                      <a href="{{ route('customer.show',$customer->id) }}" class="btn btn-primary fa fa-info-circle"></a>
                      <a href="{{ route('customer.edit',$customer->id) }}"" class="btn btn-warning fa fa-pencil-square-o"></a>
                      <!-- <a type="button" onClick="hapus();" class="btn btn-danger fa fa-trash-o"></a> -->
                    </td>
                </tr>
              @endforeach
              <!-- <script>
                function hapus(){
                  if (confirm('apakah anda yakin ingin menghapus?')) {
                      location.href = "{{ route('customer.delete',$customer->id) }}";
                  }else {
                    return false;
                  }
                }
              </script> -->
              </tbody>
              @endif
            </table>
            {!! $customers->render() !!}
@endsection