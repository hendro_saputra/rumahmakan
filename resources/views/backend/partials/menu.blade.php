<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="index.php">
            <img src="{{ asset('images/auth/restaurant.jpg') }}" class="img-circle" width="70" height="70"></a></p>
            <h5 class="centered">{{ Auth::user()->name }}</h5>
            <li class="mt">
                <a href="{{ route('index') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-book"></i>
                    <span>Makanan</span>
                </a>
                <ul class="sub active">
                    <li><a  href="{{ route('food.index') }}">Daftar Makanan</a></li>
                    <li><a  href="{{ route('food.create') }}">Tambah Makanan</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-users"></i>
                    <span>Customers</span>
                </a>
                <ul class="sub active">
                    <li><a  href="{{ route('customer.index') }}">Daftar Customer</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-car"></i>
                    <span>Driver</span>
                </a>
                <ul class="sub active">
                    <li><a  href="{{ route('driver.index') }}">Daftar Driver</a></li>
                    <li><a  href="{{ route('driver.create') }}">Tambah Driver</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-credit-card"></i>
                    <span>Data Transaksi</span>
                </a>
                <ul class="sub active">
                    <li><a  href="{{ route('order.index') }}">Order</a></li>
                    <li><a  href="{{ route('pembayaran.index') }}">Pembayaran</a></li>
                </ul>
            </li>
            <li class="sub-menu">
               <a href="{{ route('pengiriman.index') }}" >
                    <i class="fa fa-truck"></i>
                    <span>Pengiriman</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>