@extends('backend.templates.default')
@section('content')
    <form action="{{ route('driver.update',$driver->id) }}" method="post" enctype="multipart/form-data">
   {{ method_field('PUT') }}
    <h1 align="center" class="panel panel-heading">Edit Driver</h1>
        <div class="form-group row">
          <label for="name" class="col-xs-1 col-form-label">Name</label>
          <div class="col-xs-10 {{ $errors->has('name') ? ' has-error ' : '' }}">
            <input value="{{ $driver->name }}" class="form-control" type="text" name="name"  >
          @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="email" class="col-xs-1 col-form-label">Email</label>
           <div class="col-xs-10 {{ $errors->has('email') ? ' has-error ' : '' }}">
            <input value="{{ $driver->email }}" class="form-control" type="text" name="email" >
            @if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-url-input" class="col-xs-1 col-form-label">Image</label>
          <div class="col-xs-10 {{ $errors->has('image') ? ' has-error ' : '' }}">
            <input class="form-control" type="file" name="image" accept="image/*" id="example-url-input">
          @if ($errors->has('image'))
                <span class="help-block">{{ $errors->first('image') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-city-input" class="col-xs-1 col-form-label">City</label>
          <div class="col-xs-10 {{ $errors->has('city') ? ' has-error ' : '' }}">
            <input value="{{ $driver->city }}" class="form-control" name="city" type="text"  id="example-email-input">
             @if ($errors->has('city'))
                <span class="help-block">{{ $errors->first('city') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-city-input" class="col-xs-1 col-form-label">Password</label>
          <div class="col-xs-10 {{ $errors->has('password') ? ' has-error ' : '' }}">
            <input class="form-control" name="password" type="password"  id="example-email-input">
             @if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-city-input" class="col-xs-1 col-form-label">Re-Password</label>
          <div class="col-xs-10 {{ $errors->has('repassword') ? ' has-error ' : '' }}">
            <input class="form-control" name="repassword" type="password"  id="example-email-input">
             @if ($errors->has('repassword'))
                <span class="help-block">{{ $errors->first('repassword') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-gender-input" class="col-xs-1 col-form-label">Gender</label>
          <div class="col-xs-10 {{ $errors->has('gender') ? ' has-error ' : '' }}">
            <select name="gender" class="form-control">
              <option value="L" @if ($driver->gender == "L")
                  selected="selected "
                @endif>Laki-Laki</option>
              <option value="P" @if ($driver->gender == "P")
                selected="selected "
              @endif>Perempuan</option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">Alamat</label>
          <div class="col-xs-10 {{ $errors->has('address') ? ' has-error ' : '' }}">
          <textarea name="address" class="form-control">{{ $driver->address }}</textarea>
          </div>
          @if ($errors->has('address'))
                <span class="help-block">{{ $errors->first('address') }}</span>
            @endif
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">No Ktp</label>
          <div class="col-xs-10 {{ $errors->has('no_ktp') ? ' has-error ' : '' }}">
          <textarea name="no_ktp" class="form-control">{{ $driver->no_ktp }}</textarea>
          </div>
          @if ($errors->has('no_ktp'))
                <span class="help-block">{{ $errors->first('no_ktp') }}</span>
            @endif
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">No Telpon</label>
          <div class="col-xs-10 {{ $errors->has('no_telp') ? ' has-error ' : '' }}">
          <textarea name="no_telp" class="form-control">{{ $driver->no_telp }}</textarea>
          </div>
          @if ($errors->has('no_telp'))
                <span class="help-block">{{ $errors->first('no_telp') }}</span>
          @endif
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">No Sim</label>
          <div class="col-xs-10 {{ $errors->has('no_sim') ? ' has-error ' : '' }}">
          <textarea name="no_sim" class="form-control">{{ $driver->no_sim }}</textarea>
          </div>
          @if ($errors->has('no_sim'))
                <span class="help-block">{{ $errors->first('no_sim') }}</span>
            @endif
        </div>
        {{ csrf_field() }}
        <input type="submit" class="btn btn-primary ">
        <a href="{{ route('index') }}" class="btn btn-warning ">Cancel</a>
        </form>
@endsection