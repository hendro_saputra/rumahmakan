@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading text-center">Daftar Driver</h1><hr>
              <a href="{{ route('driver.create') }}" class="btn btn-primary">Tambah Driver</a>
              <div class="input-group col-md-3 pull-right">
                {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">
                  <input type="submit" value="Go">
                </span> --}}
              </div>
              <br>
              <br>
              <table class="table table-striped col-md-3">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>No KTP</th>
                  <th>Jenis Kelamin</th>
                  <th>Kota</th>
                  <th>No Telephon</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @if ($drivers->count() == 0)
                <tr>
                  <td colspan="7" align="center"><h4>Data Kosong!</h4></td>
                </tr>
              @else
              @foreach ($drivers as $driver)
                <tr>
                    <td>{{ $driver->name }}</td>
                    <td>{{ $driver->email }}</td>
                    <td>{{ $driver->no_ktp }}</td>
                    <td>{{ $driver->gender}}</td>
                    <td>{{ $driver->city }}</td>
                    <td>{{ $driver->no_telp }}</td>
                    <td>
                      <a href="{{ route('driver.show',$driver->id) }}" class="btn btn-primary fa fa-info-circle"></a>
                      <a href="{{ route('driver.edit',$driver->id) }}"" class="btn btn-warning fa fa-pencil-square-o"></a>
                      <a type="button" onClick="hapus();" class="btn btn-danger fa fa-trash-o"></a>
                    </td>
                </tr>
              @endforeach
              <script>
                function hapus(){
                  if (confirm("apakah anda yakin inginn menghapus ? ")) {
                    location.href = "{{ route('driver.delete',$driver->id) }}";
                  }
                }
              </script>
              </tbody>
              @endif
            </table>
            {!! $drivers->render() !!}
@endsection