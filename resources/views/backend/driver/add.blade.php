@extends('backend.templates.default')
@section('content')
    <form action="{{ route('driver.store') }}" method="post" enctype="multipart/form-data">
    <h1 align="center" class="panel panel-heading">Tambah Driver</h1>
        <div class="form-group row">
          <label for="name" class="col-xs-1 col-form-label">Name</label>
          <div class="col-xs-10 {{ $errors->has('name') ? ' has-error ' : '' }}">
            <input class="form-control" type="text" name="name"  >
          @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="email" class="col-xs-1 col-form-label">Email</label>
           <div class="col-xs-10 {{ $errors->has('email') ? ' has-error ' : '' }}">
            <input class="form-control" type="email" name="email" >
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="password" class="col-xs-1 col-form-label">Password</label>
          <div class="col-xs-10 {{ $errors->has('password') ? ' has-error ' : '' }}">
            <input class="form-control" type="password" name="password"  >
          @if ($errors->has('password'))
            <span class="help-block">{{ $errors->first('password') }}</span>
          @endif
          </div>
        </div>
         <div class="form-group row">
          <label for="repassword" class="col-xs-1 col-form-label">Re-Password</label>
          <div class="col-xs-10 {{ $errors->has('repassword') ? ' has-error ' : '' }}">
            <input class="form-control" type="password" name="repassword"  >
          @if ($errors->has('repassword'))
            <span class="help-block">{{ $errors->first('repassword') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="image" class="col-xs-1 col-form-label">Image</label>
          <div class="col-xs-10 {{ $errors->has('image') ? ' has-error ' : '' }}">
            <input class="form-control" type="file" name="image" accept="image/*" id="example-image-input">
            @if ($errors->has('image'))
                <span class="help-block">{{ $errors->first('image') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="city" class="col-xs-1 col-form-label">City</label>
          <div class="col-xs-10 {{ $errors->has('city') ? ' has-error ' : '' }}">
            <input class="form-control" name="city" type="text" value="" id="city">
             @if ($errors->has('city'))
                <span class="help-block">{{ $errors->first('city') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-address-input" class="col-xs-1 col-form-label">Jenis Kelamain</label>
          <div class="col-xs-10 {{ $errors->has('gender') ? ' has-error ' : '' }}">
            <select name="gender" class="form-control">
              <option value="L">Laki-Laki</option>
              <option value="P">Perempuan</option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="address" class="col-xs-1 col-form-label">Alamat</label>
          <div class="col-xs-10 {{ $errors->has('address') ? ' has-error ' : '' }}">
          <input class="form-control" name="address" type="text" value="" id="address">
          </div>
            @if ($errors->has('address'))
                <span class="help-block">{{ $errors->first('address') }}</span>
            @endif
        </div>
        <div class="form-group row">
          <label for="no_ktp" class="col-xs-1 col-form-label">No Ktp</label>
          <div class="col-xs-10 {{ $errors->has('no_ktp') ? ' has-error ' : '' }}">
          <input class="form-control" name="no_ktp" type="number" value="" id="no_ktp">
          </div>
            @if ($errors->has('no_ktp'))
                <span class="help-block">{{ $errors->first('no_ktp') }}</span>
            @endif
        </div>
        <div class="form-group row">
          <label for="no_telp" class="col-xs-1 col-form-label">No Telp</label>
          <div class="col-xs-10 {{ $errors->has('no_telp') ? ' has-error ' : '' }}">
          <input class="form-control" name="no_telp" type="number" value="" id="no_telp">
          </div>
            @if ($errors->has('no_telp'))
                <span class="help-block">{{ $errors->first('no_telp') }}</span>
            @endif
        </div>
        <div class="form-group row">
          <label for="no_sim" class="col-xs-1 col-form-label">No Sim</label>
          <div class="col-xs-10 {{ $errors->has('no_sim') ? ' has-error ' : '' }}">
          <input class="form-control" name="no_sim" type="number" value="" id="no_sim">
          </div>
            @if ($errors->has('no_sim'))
                <span class="help-block">{{ $errors->first('no_sim') }}</span>
            @endif
        </div>
        {{ csrf_field() }}
        <input type="submit" class="btn btn-primary ">
        <a href="{{ route('driver.index') }}" class="btn btn-warning ">Cancel</a>
        </form>
@endsection