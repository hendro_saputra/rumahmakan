@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Detail Driver</h1><hr>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="thumbnail">
                                <img src="{{ $driver->image }}">
                          </div>
                          <a href="{{ route('driver.index') }}" class="btn btn-primary">Kembali</a>
                      </div>
                        <div class="col-md-6">
                          <div class="panel panel-body">
                              <table class="table table-bordered">
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Nama</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $driver->name }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Email</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                    {{ $driver->email }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>City</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $driver->city }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Jenis Kelamin</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                {{ $driver->gender }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>Address</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $driver->address }}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>No Ktp</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $driver->no_ktp }}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>No Telp</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $driver->no_telp }}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>No Sim</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $driver->no_sim }}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                          </div>
                        </div>
                  </div>

@stop