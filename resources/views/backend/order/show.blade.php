@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Detail Orders</h1><hr>
      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-body">
                <table class="table table-bordered">
                  <tr>
                    <th>No</th>
                    <th>Nama Makanan</th>
                    <th>Harga Makanan normal</th>
                    <th>Diskon</th>
                    <th>Harga Makanan diskon</th>
                    <th>Jumlah</th>
                    <th>SubTotal</th>
                  </tr>
                  @foreach ($details as $detail)
                  <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $detail->food->name_food }}</td>
                      <td>{{ number_format($detail->food->price) }}</td>
                      <td>{{ $detail->food->discount }}%</td>
                      <td>{{ number_format($detail->harga_satuan) }}</td>
                      <td>{{ $detail->jumlah_barang }}</td>
                      <td>{{ number_format($detail->subtotal) }}</td>
                  </tr>
                  @endforeach
                  <tr>
                    <td></td>
                    <td colspan="4" align=""><b> Food</b></td>
                    <td><b>{{ $order->jumlah_barang }}</b></td>
                    <td ><b>{{ number_format($total) }}</b></td>
                  </tr>
                  <tr >
                    <td rowspan="4"></td>
                    <td rowspan="4" colspan="1" align=""><b> Keterangan</b></td>
                  </tr>
                    <tr>
                      <td colspan="3">Jarak</td>
                      <td>{{ ceil($order->jarak/1000) }}.Km</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">Ongkos Driver</td>
                      <td>{{ number_format($order->harga_tambahan) }}</td>
                    </tr>
                  <tr>
                    <td colspan="4"><b> Total</b></td>
                    <td ><b>{{ number_format($order->total) }}</b></td>
                  </tr>
                </table>
            </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-6">
              <a href="{{ route('order.index') }}" class="btn btn-primary">Kembali</a>
          </div>
      </div>
@stop