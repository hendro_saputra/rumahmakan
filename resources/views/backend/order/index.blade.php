@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Daftar Orders</h1><hr>
    <div class="input-group col-md-3 pull-right">
      {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
      <span class="input-group-addon" id="basic-addon2">
        <input type="submit" value="Go">
      </span> --}}
    </div>
    <br>
    <br>
    <table class="table table-striped col-md-3">
    <thead>
      <tr>
        <th>Nama </th>
        <th>Email </th>
        <th>Total Barang</th>
        <th>Order Details</th>
        <th>Total Harga</th>
        <th>Tanggal Order</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @if ($orders->count() == 0)
      <tr>
        <td colspan="6" align="center"><h4>Data Order Kosong!</h4></td>
      </tr>
    @else
    @foreach ($orders as $order)
      <tr>
          <td>{{ $order->customer->name }}</td>
          <td>{{ $order->customer->email }}</td>
          <td>{{ $order->jumlah_barang }}</td>
          <td>
            <a href="{{ route('order.show',$order->id) }}" class="btn btn-primary">View</a>
          </td>
          <td>Rp.{{ number_format($order->total) }}</td>
          <td>{{ $order->tanggal }}</td>
          <td>
          <a type="button" onClick="hapus();" class="btn btn-danger fa fa-trash-o hapus"></a>
          </td>
      </tr>
    @endforeach
    <script>
      function hapus(){
        if (confirm("apakah anda yakin inginn menghapus ? ")) {
          location.href = "{{ route('order.delete',$order->id) }}";
        }
      }
    </script>
    </tbody>
    @endif
  </table>
  {!! $orders->render() !!}
@endsection