@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Daftar Makanan</h1><hr>
      <div class="form-group">
        <a href="{{ route('food.create') }}" class="btn btn-primary">Tambah Makanan</a>
          <form action="" method="get" accept-charset="utf-8">
            <div class="col-md-2 col-xs-offset-9">
            <input type="text" name="name" value="{{ Request::old('name') }}" placeholder="Nama.." class="form-control">
          </div>
          <input type="submit" name="" value="Cari" class="col-md-1 btn btn-primary pull-right">
          </form>
      </div>
      <div class="input-group col-md-3 pull-right">
        {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
        <span class="input-group-addon" id="basic-addon2">
          <input type="submit" value="Go">
        </span> --}}
      </div>
      <br>
      <br>
      <table class="table table-striped col-md-3">
      <thead>
        <tr>
          <th>Nama Makanan</th>
          <th>Harga</th>
          <th>Stok</th>
          <th>Diskon</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      @if ($foods->count() == 0)
        <tr>
          <td colspan="5" align="center"><h4>Data Kosong!</h4></td>
        </tr>
      @else
      @foreach ($foods as $food)
        <tr>
            <td>{{ $food->name_food }}</td>
            <td>Rp.{{ number_format($food->price) }}</td>
            <td>{{ $food->stock }}</td>
            <td>{{ $food->discount }}%</td>
            <td>
            <a href="{{ route('food.show',$food->id) }}" class="btn btn-primary fa fa-info-circle"></a>
            <a href="{{ route('food.edit',$food->id) }}" class="btn btn-warning fa fa-pencil-square-o"></a>
            <!-- <a type="button" onClick="hapus();" class="btn btn-danger fa fa-trash-o hapus"></a> -->
            </td>
        </tr>
      @endforeach
      <!-- <script>
        function hapus(){
          if (confirm("apakah anda yakin inginn menghapus ? ")) {
            location.href = "{{ route('food.delete',$food->id) }}";
          }
        }
      </script> -->
      </tbody>
      @endif
    </table>
    {!! $foods->appends([
        'name' => Request::get('name'),
      ])->render() !!}
@endsection