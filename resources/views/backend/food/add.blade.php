@extends('backend.templates.default')
@section('content')
    <form action="{{ route('food.store') }}" method="post" enctype="multipart/form-data">
    <h1 align="center" class="panel panel-heading">Tambah Makanan</h1>
        <div class="form-group row">
          <label for="name_food" class="col-xs-1 col-form-label">Name</label>
          <div class="col-xs-10 {{ $errors->has('name_food') ? ' has-error ' : '' }}">
            <input class="form-control" type="text" name="name_food"  >
          @if ($errors->has('name_food'))
            <span class="help-block">{{ $errors->first('name_food') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="price" class="col-xs-1 col-form-label">Price</label>
           <div class="col-xs-10 {{ $errors->has('price') ? ' has-error ' : '' }}">
            <input class="form-control" type="number" name="price" >
            @if ($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-url-input" class="col-xs-1 col-form-label">Image</label>
          <div class="col-xs-10 {{ $errors->has('image') ? ' has-error ' : '' }}">
            <input class="form-control" type="file" name="image" accept="image/*" id="example-url-input">
          @if ($errors->has('image'))
                <span class="help-block">{{ $errors->first('image') }}</span>
          @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">Diskon</label>
          <div class="col-xs-10 {{ $errors->has('discount') ? ' has-error ' : '' }}">
            <input class="form-control" name="discount" type="number" value="" id="example-email-input">
             @if ($errors->has('discount'))
                <span class="help-block">{{ $errors->first('discount') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-number-input" class="col-xs-1 col-form-label">Stok</label>
          <div class="col-xs-10 {{ $errors->has('stock') ? ' has-error ' : '' }}">
            <input class="form-control" name="stock" type="number" value="" id="example-number-input">
             @if ($errors->has('stock'))
                <span class="help-block">{{ $errors->first('stock') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="example-email-input" class="col-xs-1 col-form-label">Deskripsi</label>
          <div class="col-xs-10 {{ $errors->has('description') ? ' has-error ' : '' }}">
          <textarea name="description" class="form-control"></textarea>
          </div>
          @if ($errors->has('description'))
                <span class="help-block">{{ $errors->first('description') }}</span>
            @endif
        </div>
        {{ csrf_field() }}
        <input type="submit" class="btn btn-primary ">
        <a href="{{ route('food.index') }}" class="btn btn-warning ">Cancel</a>
        </form>
@endsection