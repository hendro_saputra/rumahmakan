@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Detail Makanan</h1><hr>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="thumbnail">
                                <img src="{{ $food->image }}" alt="">
                          </div>
                          <a href="{{ route('food.index') }}" class="btn btn-primary">Kembali</a>
                      </div>
                        <div class="col-md-6">
                          <div class="panel panel-body">
                              <table class="table table-bordered">
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Nama Makanan</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $food->name_food }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Harga</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                Rp.{{ number_format($food->price) }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Diskon</b></p>
                                            </td>
                                            <td>
                                                <p>{{ $food->discount }}</p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <p>
                                            <td>
                                                <p><b>Deskripsi</b></p>
                                            </td>
                                            <td>
                                                <p>
                                                {{ $food->description }}
                                                </p>
                                            </td>
                                        </p>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><b>Stok</b></p>
                                        </td>
                                        <td>
                                            <p>
                                            {{ $food->stock }}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                          </div>
                        </div>
                  </div>

@stop