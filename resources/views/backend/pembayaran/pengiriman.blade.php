@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Daftar Pengiriman</h1><hr>
    <div class="input-group col-md-3 pull-right">
      {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
      <span class="input-group-addon" id="basic-addon2">
        <input type="submit" value="Go">
      </span> --}}
    </div>
    <br>
    <br>
    <table class="table table-striped col-md-3">
    <thead>
      <tr>
        <th>No</th>
        <th>No Faktur</th>
        <th>Driver Pengirim</th>
        <th>Alamat Penerima</th>
        <th>Nama Penerima</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    @if ($pengirimans->count() == 0)
      <tr>
        <td colspan="11" align="center"><h4>Data pengiriman Kosong!</h4></td>
      </tr>
    @else
    @php
      $i=1;
    @endphp
    @foreach ($pengirimans as $pengiriman)
      <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $pengiriman->pembayaran_id }}</td>
          <td>{{ $pengiriman->driver->name }}</td>
          <td>{{ $pengiriman->alamat_penerima }}</td>
          <td>{{ $pengiriman->nama_penerima }}</td>
          <td>
            @if ($pengiriman->status == 0)
                <p>
                <i><b>{{ 'Sedang Mengirim!' }}</b></i>
                </p>
           @elseif ($pengiriman->status == 1)
                <a ><i><b>{{ 'Terkirim' }}</b></i></a>
           @endif
          </td>
      </tr>
    @endforeach
    </tbody>
    @endif
  </table>
  {!! $pengirimans->render() !!}
@endsection