@extends('backend.templates.default')
@section('content')
    <h1 class="panel panel-heading">Daftar Pembayaran</h1><hr>
    <div class="input-group col-md-3 pull-right">
      {{-- <input type="text" class="form-control" placeholder="Search.." aria-describedby="basic-addon2">
      <span class="input-group-addon" id="basic-addon2">
        <input type="submit" value="Go">
      </span> --}}
    </div>
    <br>
    <br>
    <table class="table table-striped col-md-3">
    <thead>
      <tr>
        <th>Faktur</th>
        <th>Nama </th>
        <th>Payment</th>
        <th>Payment Type</th>
        <th>ID Unik</th>
        <th>Total</th>
        <th>Tanggal Bayar</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    @if ($pembayaran->count() == 0)
      <tr>
        <td colspan="11" align="center"><h4>Data bayar Kosong!</h4></td>
      </tr>
    @else
    @foreach ($pembayaran as $bayar)
      <tr>
          <td>{{ $bayar->id }}</td>
          <td>{{ $bayar->order->customer->name }}</td>
          <td>{{ $bayar->paymentMethod->name }}</td>
          <td>{{ $bayar->payment_type }}</td>
          <td>{{ $bayar->unique_id }}</td>
          <td>{{ $bayar->total }}</td>
          <td>{{ $bayar->created_at }}</td>
          <td>
            @if ($bayar->payment_status == 0)
                {{ 'Belom Bayar' }}
                <a href="{{ route('pembayaran.change',[$bayar->id,1,$bayar->order_id]) }}" class="btn btn-default">Aktifasi</a>
           @elseif($bayar->payment_status == 1)
                {{ 'Sudah Bayar' }}
           @elseif ($bayar->payment_status == 2)
                {{ 'Dikirim' }}
           @elseif ($bayar->payment_status == 3)
                {{ 'Transaksi Sukses' }}
           @endif
          </td>
      </tr>
    @endforeach
    </tbody>
    @endif
  </table>
  {!! $pembayaran->render() !!}
@endsection